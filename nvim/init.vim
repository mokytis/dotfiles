set noerrorbells    " Who actually wants the 'ding' noise?
set encoding=utf-8  " UTF-8 encoding for files


" Indentation. Tabs Not Spaces
set tabstop=4       " tabs appear 4 spaces wide
set softtabstop=4   " size of a tab when <tab> is pressed in insert mode
set shiftwidth=4    " show far to indent when 'shifting' rows
set expandtab       " tab key should insert spaces
set smartindent     " automatically indent when appropriate


" Line Numbers
set number          " show line numbers
set relativenumber  " current line = actual number;
                    " other lines = distance from current

" Search
set ignorecase      " ignore case when searching
set smartcase       " unless upper case is used, then search case
                    " (lowercase only = case insensitive search)

" Color Column
set colorcolumn=80  " line at col=80
highligh ColorColumn ctermbg=0 guibg=lightgrey


" Plugins
call plug#begin()
" python autoformatter
    Plug 'psf/black', { 'tag': '19.10b0' }
" LaTeX support
    Plug 'lervag/vimtex'
" git intergration
    Plug 'tpope/vim-fugitive'
" javascript indentation & syntax highlighting
    Plug 'pangloss/vim-javascript'
" react js jsx indentation & syntax highlighting
    Plug 'mxw/vim-jsx'
" Jinja2 syntax highlighting
    Plug 'Glench/Vim-Jinja2-Syntax'
" go intergration
    Plug 'fatih/vim-go'
" emmet for vim
    Plug 'mattn/emmet-vim'
" toml syntax highlighting
    Plug 'cespare/vim-toml'
" a personal wiki, in vim
    Plug 'vimwiki/vimwiki'
" the only good colour scheme (solarized)
    Plug 'altercation/vim-colors-solarized'
" view and search man pages in vim
    Plug 'vim-utils/vim-man'
" autocomplete engine
    Plug 'Shougo/deoplete.nvim' , { 'do': ':UpdateRemotePlugins' }
" python autocomplete
    Plug 'zchee/deoplete-jedi', { 'for': 'python' }
" python definitions
    Plug 'davidhalter/jedi-vim', { 'for': 'python' }
" file tree
    Plug 'preservim/nerdtree'
call plug#end()


" Autocomplete
let g:deoplete#enable_at_startup = 1    " autostart autocompleter


" Colour Scheme
colorscheme solarized   " use solarized dark colour scheme


" Keybinds
let mapleader = " "                     " set leaderkey to <space>
nnoremap <leader>h :wincmd h<CR>        " moves to left window
nnoremap <leader>j :wincmd j<CR>        " moves down a window
nnoremap <leader>k :wincmd k<CR>        " moves up a window
nnoremap <leader>l :wincmd l<CR>        " moves to right window
nnoremap <silent> <leader>+ :vertical resize +5<CR> " inc current window size
nnoremap <silent> <leader>- :vertical resize -5<CR> " dec current window size
nnoremap <leader>t :NERDTreeToggle<CR>  " toggle file tree


" Unwanted Whitespace
highlight TrailingWhitespace ctermbg=red    " highlight trailing whitespace red
match TrailingWhitespace /\s\+$/            " pattern for trainling whitespace


" Black
autocmd BufWritePre,FileWritePre *.py execute ':Black'


" Vimwiki
let g:vimwiki_list = [{'path': '~/files/vimwiki/'}]  " Where vimwiki stores files
