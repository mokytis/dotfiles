#!/bin/sh

git submodule update --init --recursive

DOTFILES_DIR=$(pwd)
NVIM_CONF_DIR="${XDG_CONFIG:-$HOME/.config}"/nvim

function install_config {
	SRC=$1
	DEST=$2

	if [ -L "$DEST" ]; then
	    echo "[-] A SYMLINK to $DEST already exists"
	else
	    ln -s $SRC $DEST
	    echo "[+] Created SYMLINK to $DEST"
	fi
}

# nvim
curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
install_config $DOTFILES_DIR/nvim $NVIM_CONF_DIR


# tmux
install_config $DOTFILES_DIR/tmux/.tmux.conf $HOME/.tmux.conf

install_config $DOTFILES_DIR/bash/.bashrc $HOME/.bashrc
install_config $DOTFILES_DIR/bash/.profile $HOME/.profile
