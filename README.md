# Dotfiles

This repository contains all of my current configuration files.

Just create symlinks to install or do this automaticaly by running `./install.sh`

## Suckless config?

My suckless forks are all sotred in seperate repos.

* dwm - [gitlab.com/mokytis/dwm](https://gitlab.com/mokytis/dwm/)
* st - [gitlab.com/mokyis/st](https://gitlab.com/mokytis/st/)
* slstatus - [gitlab.com/mokytis/slstatus](https://gitlab.com/mokytis/slstatus/)
