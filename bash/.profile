
export PATH="$HOME/.cargo/bin:$PATH"

# XDG_DIR
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.data

## iPython
export IPYTHONDIR=$XDG_CONFIG_HOME/ipython
export JUPYTER_CONFIG_DIR=$XDG_CONFIG_HOME/jupyter

## Ruby
export BUNDLE_USER_CONFIG=$XDG_CONFIG_HOME/bundle
export BUNDLE_USER_CACHE=$XDG_CONFIG_HOME/bundle
export GEM_HOME=$XDG_CONFIG_HOME/gem
export GEM_SPEC_CACHE=$XDG_CACHE_HOME/gem

## Rust
export CARGO_HOME=$HOME/.cargo

# Less
export LESSHISTFILE="-"  # don't want less history file

# pyenv
export PYENV_ROOT=$XDG_CONFIG_HOME/pyenv
export PATH=$PYENV_ROOT/bin:$PATH
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

. ~/.bashrc
