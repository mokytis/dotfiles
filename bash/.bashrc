# Setting up PATH

export GOPATH=$HOME/files/code/go
export SCRIPTS_DIR=$HOME/src/scripts
export JAVA_HOME=/usr/lib/jvm/java-13-openjdk/
export IMPACKET_EXAMPLES=$HOME/src/impacket/examples

PATH=$GOPATH/bin:$PATH
PATH=$SCRIPTS_DIR:$PATH
PATH=$JAVA_HOME/bin:$PATH
PATH=$IMPACKET_EXAMPLES:$PATH
PATH=$GEM_HOME/bin:$PATH
PATH=$CARGO_HOME/bin:$PATH
PATH=$HOME/.local/bin:/usr/local/bin:$PATH

export PATH

# gnupg

export GPG_TTY="$(tty)"
unset SSH_AGENT_PID
export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
gpgconf --launch gpg-agent


# Useful functions

function renewdhcplease() {
	# gets a new dhcp lease from dhcp server
	sudo dhclient -r $1
	sudo dhclient $1
}

function flash_microbit() {
	# flashes code to microbit
	echo "Mounting microbit..."
	sudo mount /dev/sda ~/microbit
	sudo uflash $1 ~/microbit
	echo "Unmountint microbit..."
	sudo umount ~/microbit
}

function mit() {
    year=$(date +%Y)
    cat << EOF
Copyright $year Luke Spademan

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
EOF
}


# neovim

export VISUAL=nvim
export EDITOR="$VISUAL"
alias v=nvim
alias vim=nvim
alias jl="(cd $HOME/files/code/python/notebooks && poetry run jupyter-lab)"


# misc

export MANPAGER="sh -c 'col -bx | bat -l man -p'"	# use bat for manpages
alias vlcwc="vlc v4l2:///dev/video0"			# open webcam in vlc

alias ls='ls --color=auto'

source $HOME/prompt.sh

set -o vi
bind "set completion-ignore-case on"
PROMPT_DIRTRIM=2
bind Space:magic-space
shopt -s globstar 2> /dev/null
shopt -s nocaseglob;
bind "set show-all-if-ambiguous on"
# history
shopt -s histappend
shopt -s cmdhist
PROMPT_COMMAND='history -a'
export HISTIGNORE="&:[ ]*:exit:ls:bg:fg:history:clear"
HISTTIMEFORMAT='%F %T '
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
bind '"\e[C": forward-char'
bind '"\e[D": backward-char'

bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'
export NODE_OPTIONS=--max_old_space_size=2048
